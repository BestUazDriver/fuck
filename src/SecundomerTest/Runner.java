package SecundomerTest;

import java.util.Random;

public class Runner extends Thread {
    int oTime;

    @Override
    public void run() {
        int  totalTime=0;
        Random random = new Random();
        System.out.println("Start");
        for (int i = 0; i < 5; i++) {
            int lapTime = random.nextInt(3000);
            try {
                sleep(lapTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("runner reached point number "+ (i+1));
            System.out.println("lap time: "+ lapTime+ " ms");
            System.out.println();
            totalTime = totalTime + lapTime;
            oTime=totalTime;
            if (totalTime>5000){
                try {
                    System.out.println("STOPP");
                    new MainThread().join();
                    sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (totalTime<5000){
            System.out.println("ruuner got on time");
            System.out.println("total time: "+totalTime);
        }
    }
}
