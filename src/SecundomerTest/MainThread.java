package SecundomerTest;

public class MainThread extends Thread{
    @Override
    public void run() {
        Runner runner = new Runner();
        runner.start();
        try {
            runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (runner.oTime>5000){
            runner.interrupt();
            System.out.println("runner out of time");
            System.out.println("total time: " + runner.oTime + " ms");
        }

    }
}
