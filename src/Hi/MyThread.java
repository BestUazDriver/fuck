package Hi;
import java.util.Random;

public class MyThread extends Thread{
    private String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < random.nextInt(5); i++) {
            try {
                sleep(random.nextInt(5000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);
        }
    }
}